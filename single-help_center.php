<?php
/**
 * The template for displaying Help Center single post
 */
?>

<?php get_header(); ?>

<?php
$terms = get_terms([
  'taxonomy' => 'section',
  'hide_empty' => false,
]);
$article_id = get_the_ID();
$article_tabs = get_field('tabs');
?>

  <main class="Main">
    <div class="Main-wrapper">
      <div class="Main-sidebar">
        <div class="Sidebar">
          <?php foreach ($terms as $term_item) :
            $articles_args = [
              'post_type' => 'help_center',
              'posts_per_page' => -1,
              'tax_query' => [
                [
                  'taxonomy' => 'section',
                  'field'    => 'term_id',
                  'terms'    => $term_item->term_id,
                ],
              ],
              'order' => 'ASC',
              'orderby' => 'menu_order'
            ];

            $articles = new WP_Query($articles_args);
          ?>
            <div class="Sidebar-item">
              <span class="Sidebar-itemHeading"><?php echo esc_html($term_item->name); ?></span>

              <ul class="Sidebar-list">
                <?php while ($articles->have_posts()) : $articles->the_post(); ?>
                  <?php $active_class = $article_id === get_the_ID() ? '-active' : null; ?>

                  <li class="Sidebar-listItem <?php echo esc_attr($active_class); ?>">
                    <a class="Sidebar-listItemLink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </li>
                <?php endwhile; wp_reset_postdata(); ?>
              </ul>
            </div>
          <?php endforeach; ?>
        </div>
      </div>

      <?php if (!empty($article_tabs)) : ?>
        <div class="Main-body">
          <div class="Article">
            <div class="Article-header">
              <div class="Article-back">
                <a class="Article-backBtn" href="<?php echo esc_url(home_url('/help-center')); ?>">See all articles</a>
              </div>
              <h1 class="Article-title"><?php the_title(); ?></h1>

              <?php if (count($article_tabs) > 1) : ?>
                <ul class="Article-tabs">
                  <?php foreach ($article_tabs as $key => $article_tab) :
                    $tab_title = $article_tab['title'];
                    $active_class = $key === 0 ? '-active' : null; ?>
                    <li class="Article-tabsItem <?php echo esc_attr($active_class); ?>">
                      <button class="Article-tabsItemBtn" data-tab="<?php echo esc_attr(sanitize_title($tab_title)); ?>"><?php echo esc_html($tab_title); ?></button>
                    </li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </div>
            <div class="Article-body">
              <?php while (have_rows('tabs')) :
                the_row();
                $tab_title = get_sub_field('title');
                $active_class =  get_row_index() === 1 ? '-active' : null; ?>
                <div class="Article-item <?php echo esc_attr($active_class); ?>" data-item="<?php echo esc_attr(sanitize_title($tab_title)); ?>">
                  <?php while (have_rows('components')) :
                    the_row();
                    $section_name = str_replace('_', '-', get_row_layout()); ?>

                    <?php get_template_part("views/components/help-center/{$section_name}"); ?>

                  <?php endwhile; ?>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </main>

<?php get_footer();
