<?php
/**
 * 404 template file
 */
?>

<?php get_header(); ?>

<main class="Main">
  <div class="Main-components">
    <section class="Page404">
      <div class="Page404-wrapper">
        <span class="Page404-title">404</span>
        <span class="Page404-subtitle">Page not found!</span>
        <p class="Page404-text">Oops! Something has gone wrong and the page you were looking for could not be displayed!</p>
        <a class="Page404-link" href="<?php echo esc_url(home_url()); ?>">Go Homepage</a>
      </div>
    </section>
  </div>
</main>

<?php get_footer();
