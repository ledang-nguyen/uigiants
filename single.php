<?php
/**
 * The template for displaying all single posts and attachments
 */
?>

<?php get_header(); ?>

<?php
$current_tags = get_the_tags() ? get_the_tags() : [];
$tag_names = [];
$tag_ids = [];

foreach ($current_tags as $current_tag) {
  array_push($tag_ids, $current_tag->term_id);
  array_push($tag_names, $current_tag->name);
}

$features_posts_args = [
  'posts_per_page' => 2,
  'tag__in' => $tag_ids,
  'post__not_in' => [get_the_ID()],
];
$featured_posts = new WP_Query($features_posts_args);
?>

<main class="Main">
  <div class="Main-components">
    <div class="Single">
      <div class="Single-wrapper">
        <div class="Single-header">
          <h1 class="Single-title">
            <?php the_title(); ?>
          </h1>
          <span class="Single-date">
            <?php the_date('F j, Y'); ?>
          </span>
          <div class="Single-thumbnail">
            <?php the_post_thumbnail('large'); ?>
          </div>
        </div>
        <div class="Single-body">
          <?php while (have_rows('components')) :
            the_row();
            $section_name = str_replace('_', '-', get_row_layout()); ?>

            <?php get_template_part("views/components/blog/{$section_name}"); ?>

          <?php endwhile; ?>
        </div>
        <div class="Single-footer">
          <?php if (!empty($tag_names)) : ?>
            <ul class="Single-tags">
              <?php foreach ($tag_names as $tag_name) : ?>
                <li class="Single-tagsItem"><?php echo esc_html($tag_name); ?></li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>

          <ul class="Single-share">
            <li class="Single-shareItem -twitter">
              <a href="https://twitter.com/intent/tweet?text=Check this out! <?php the_permalink(); ?> " target="_blank" rel="noopener">
                <i class="icon-twitter" aria-hidden="true"></i>
              </a>
            </li>
            <li class="Single-shareItem -pinterest">
              <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php the_post_thumbnail_url('full'); ?>&description=Check this out!" target="_blank" rel="noopener">
                <i class="icon-pinterest" aria-hidden="true"></i>
              </a>
            </li>
            <li class="Single-shareItem -facebook">
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" rel="noopener">
                <i class="icon-facebook" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <?php if ($featured_posts->have_posts()) : ?>
      <div class="Posts">
        <div class="Posts-wrapper">
          <div class="Posts-header">
            <span class="Posts-title">You May Also Like</span>
          </div>
          <div class="Posts-body">
            <?php while ($featured_posts->have_posts()) : $featured_posts->the_post(); ?>
              <div class="Posts-item Posts-item-<?php echo esc_attr(get_the_ID()); ?>">
                <div class="Posts-itemInner">
                  <div class="Posts-itemImage">
                    <a class="Posts-itemImageLink" href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('single-posts'); ?>
                    </a>
                  </div>
                  <div class="Posts-itemContent">
                    <h3 class="Posts-itemTitle">
                      <a class="Posts-itemTitleLink" href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                      </a>
                    </h3>
                    <div class="Posts-itemExcerpt">
                      <?php the_excerpt(); ?>
                    </div>
                    <a class="Posts-itemLink" href="<?php the_permalink(); ?>">Read More</a>
                  </div>
                </div>
              </div>
            <?php endwhile; wp_reset_postdata(); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</main>

<?php get_footer();
