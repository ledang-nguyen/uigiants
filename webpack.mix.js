const mix = require('laravel-mix');
const path = require('path');
const os = require('os');

// Variables
const proxy = 'http://uigiants/';
const publicPath = os.platform() === 'win32' ? path.normalize('assets/build') : 'assets/build';

// Settings
mix
  .setPublicPath(publicPath)
  .options({
    processCssUrls: false,
    postCss: [
      require('autoprefixer'),
      require('postcss-custom-media'),
      require('postcss-assets'),
      require('css-mqpacker')({
        sort: true,
      }),
    ],
    autoprefixer: {
      options: {
        grid: true,
      },
    },
  });

// Assets build and copying
mix
  .sass('assets/src/scss/entry.scss', 'css/bundle.css')
  .js('assets/src/js/entry.js', 'js/bundle.js')
  .copy('assets/src/fonts', `${publicPath}/fonts`)
  .copy('node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css', `${publicPath}/css`)
  .copy('node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js', `${publicPath}/js`)
  .copy('node_modules/jquery/dist/jquery.min.js', `${publicPath}/js`)
  .copy('node_modules/jquery-migrate/dist/jquery-migrate.min.js', `${publicPath}/js`)
  .sourceMaps(false, 'inline-source-map');

if (mix.inProduction()) {
  mix.version();
}

// JavaScript global variables
// mix.autoload({
//   jquery: ['$', 'jQuery'],
// });

// BrowserSync setup
mix.browserSync({
  proxy,
  ui: false,
  open: true,
  notify: false,
  ghostMode: {
    clicks: false,
    forms: false,
    scroll: false,
  },
  files: [
    '*.php',
    'views/*.php',
    'views/**/*.php',
    'functions/*.php',
    'functions/**/*.php',
    'assets/build/css/bundle.css',
    'assets/build/js/bundle.js',
  ],
  snippetOptions: {
    whitelist: ['/wp-admin/admin-ajax.php'],
    blacklist: ['/wp-admin/**'],
  },
});

// Disable OS notifications
mix.disableSuccessNotifications();
