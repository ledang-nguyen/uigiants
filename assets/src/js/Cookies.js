jQuery(($) => {
  class Cookies {
    constructor() {
      this.$container = $('.Cookies');
      this.$button = $('.Cookies-button');
      this.cookies = () => {
        return localStorage.getItem('cookies');
      }
    }

    setItem() {
      this.$button.on('click', () => {
        localStorage.setItem('cookies', 'true');

        this.hideBox();
      });
    }

    hideBox() {
      if (this.cookies()) {
        this.$container.removeClass('-visible');
      }
    }

    showBox() {
      if (!this.cookies()) {
        this.$container.addClass('-visible');
      }
    }

    init() {
      this.setItem();
      this.showBox();
    }
  }

  return new Cookies().init();
});
