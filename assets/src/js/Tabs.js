jQuery(($) => {
  class Tabs {
    constructor() {
      this.$btnWrapper = $('.Article-tabsItem');
      this.$btn = $('.Article-tabsItemBtn');
      this.$item = $('.Article-item');
    }

    click() {
      this.$btn.on('click', (e) => {
        const $btn = $(e.currentTarget);
        const $btnWrapper = $btn.parent();
        const tabName = $btn.data('tab');
        const $item = $(`[data-item="${tabName}"]`);

        if (!$btnWrapper.hasClass('-active')) {
          this.$btnWrapper.removeClass('-active');
          this.$item.removeClass('-active');

          $btnWrapper.addClass('-active');
          $item.addClass('-active');
        }
      })
    }

    init() {
      this.click();
    }
  }

  return new Tabs().init();
})
