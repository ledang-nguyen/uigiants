jQuery(($) => {
  class ContactForm {
    constructor() {
      this.$form = $('.ContactUs');
      this.defaultBtnText = 'Send';
      this.loadingBtnText = 'Please wait...';
      this.$submitBtn = $('.ContactUs .wpcf7-submit');
    }

    thankYou(that) {
      return () => {
        const breakpoint = window.matchMedia('(max-width: 767px)');

        if (breakpoint.matches) {
          $.fancybox.open({
            src: '.ContactUs-thankYou',
            type: 'inline',
          });
        } else {
          that.$form.addClass('-thankYou');
        }
      }
    }

    loading() {
      this.$form.on('click', '.wpcf7-submit', (e) => {
        $(e.currentTarget)
          .val(this.loadingBtnText)
          .addClass('-loading');
      });

      this.$form.on('wpcf7submit', () => {
        this.$submitBtn
          .val(this.defaultBtnText)
          .removeClass('-loading');
      })
    }

    init() {
      this.loading();

      window.thankYou = this.thankYou(this);
    }
  }

  return new ContactForm().init();
});
