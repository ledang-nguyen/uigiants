jQuery(($) => {
  class TopBtn {
    constructor() {
      this.$btn = $('.TopBtn-button');
    }

    clickEvent() {
      this.$btn.on('click', () => {
        $('html, body').animate({
          scrollTop: 0,
        }, 600);
      });
    }

    init() {
      if (this.$btn.length > 0) {
        this.clickEvent();
      }
    }
  }

  return new TopBtn().init();
});
