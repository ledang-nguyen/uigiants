jQuery(($) => {
  class TextWithMedia {
    constructor() {
      this.$videoContainer = $('.TextWithMedia-media');
      this.playVideo = (video) => {
        if (video && video.readyState > 0) {
          video.play();
        }
      };
      this.pauseVideo = video => {
        if (video && video.readyState > 0) {
          video.pause();
        }
      };
    }

    scrollEvent(el) {
      const $container = $(el);
      const video = $container.find('video').get(0);

      $(window).on('scroll', () => {
        const $containerOffset = $container.offset().top;
        const $containerHeight = $container.innerHeight();

        if (window.pageYOffset + (window.innerHeight / 2) >= $containerOffset && window.pageYOffset + (window.innerHeight / 2) <= ($containerOffset + $containerHeight)) {
          this.playVideo(video);
        } else {
          this.pauseVideo(video);
        }
      });
    }

    init() {
      if (this.$videoContainer.length > 0) {
        this.$videoContainer.each((index, el) => {
          this.scrollEvent(el);
        });
      }
    }
  }

  return new TextWithMedia().init();
});
