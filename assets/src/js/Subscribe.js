jQuery(($) => {
  class Subscribe {
    constructor() {
      this.$button = $('.js-subscribe');
      this.$container = $('.Subscribe');
    }

    clickEvent() {
      this.$button.on('click', () => {
        $('html, body').animate({
          scrollTop: this.$container.offset().top,
        }, 800)
      });
    }

    init() {
      this.clickEvent();
    }
  }

  return new Subscribe().init();
});
