<?php
/**
 * Template Name: Thank You Template
 */
?>

<?php get_header(); ?>

<?php
$heading = get_field('title');
$text = get_field('text');
$back_link = get_field('link');
?>

<main class="Main">
  <div class="Main-components">
    <section class="ThankYou">
      <div class="ThankYou-wrapper">
        <span class="ThankYou-icon"></span>
        <span class="ThankYou-title">
          <?php echo esc_html($heading); ?>
        </span>
        <div class="ThankYou-text">
          <?php echo wp_kses_post($text); ?>
        </div>
        <?php get_field_link($back_link, 'ThankYou-link', 'Got it'); ?>
      </div>
    </section>
  </div>
</main>

<?php get_footer();
