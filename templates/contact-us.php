<?php
/**
 * Template Name: Contact Us Template
 */
?>

<?php get_header(); ?>

<?php
enqueue_fancybox();

$heading = get_field('title');
$text = get_field('text');
$form_id = get_field('form_id');
$thank_you = get_field('thank_you');
$socials = $thank_you['socials'];
?>

<div class="Main">
  <div class="Main-components">
    <div class="ContactUs">
      <div class="ContactUs-wrapper">
        <div class="ContactUs-header">
          <h1 class="ContactUs-title">
            <?php echo esc_html($heading); ?>
          </h1>
          <div class="ContactUs-text">
            <?php echo wp_kses_post($text) ?>
          </div>
        </div>
        <div class="ContactUs-body">
          <div class="ContactUs-form">
            <?php echo do_shortcode("[contact-form-7 id=\"${form_id}\"]"); ?>
          </div>
          <div class="ContactUs-thankYou">
            <span class="ContactUs-thankYouTitle">
              <?php echo esc_html($thank_you['title']); ?>
            </span>
            <div class="ContactUs-thankYouText">
              <?php echo wp_kses_post($thank_you['text']); ?>
            </div>

            <?php if (!empty($socials)) : ?>
              <ul class="ContactUs-socials">
                <?php foreach ($socials as $social) :
                  $social_type = $social['type'];
                  $social_link = $social['link'];
                  ?>
                  <li class="ContactUs-socialsItem">
                    <?php get_field_link($social_link, "ContactUs-socialsLink -${social_type}"); ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>

            <a href="<?php the_permalink(); ?>" class="ContactUs-thankYouBack">Submit Another Email</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
