<?php
/**
 * The template for displaying the header
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T6LK2SJ');</script>
<!-- End Google Tag Manager -->

  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
  <link rel="preload" href="/wp-content/themes/uigiants/assets/build/fonts/AvenirNext-Regular.woff2" as="font"  crossorigin="anonymous">
  <link rel="preload" href="/wp-content/themes/uigiants/assets/build/fonts/AvenirNext-Medium.woff2" as="font"  crossorigin="anonymous">
  <link rel="preload" href="/wp-content/themes/uigiants/assets/build/fonts/AvenirNext-DemiBold.woff2" as="font"  crossorigin="anonymous">
  <link rel="preload" href="/wp-content/themes/uigiants/assets/build/fonts/AvenirNext-Bold.woff2" as="font"  crossorigin="anonymous">

  <?php wp_head(); ?>
 <meta name="google-site-verification" content="gWbm8fTuHRJeatn12J5AQiaXRP1n9X9E50t1WNUo-y4">
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6LK2SJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php get_template_part('views/global/header'); ?>

