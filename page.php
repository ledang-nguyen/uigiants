<?php
/**
 * The default page template file
 */
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
  <main class="Main">
    <div class="Main-components">
      <div class="Content">
        <div class="Content-wrapper">
          <?php while (have_posts()) : the_post(); ?>
            <div class="Content-header">
              <h1 class="Content-title"><?php the_title(); ?></h1>
              <span class="Content-date">Last Updated: <?php echo esc_html(get_the_modified_time('F jS, Y')); ?></span>
            </div>
            <div class="Content-body">
              <?php the_content(); ?>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </main>
<?php endif; ?>

<?php get_footer();
