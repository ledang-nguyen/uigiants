<?php

/**
 * Register social links customizer fields
 */
add_action('customize_register', function ($exclude) {
  global $wp_customize;

  $socials = [
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'dribbble' => 'Dribbble',
  ];

  $wp_customize->add_section('social_links', [
    'title' => __('Social Links'),
    'priority' => 30,
  ]);

  array_walk($socials, function ($value, $key) use ($wp_customize) {
    $wp_customize->add_setting("social_links[{$key}]", [
      'capability' => 'edit_theme_options',
      'default' => '',
      'sanitize_callback' => 'esc_url_raw',
    ]);

    $wp_customize->add_control("social_links[{$key}]", [
      'type' => 'url',
      'section' => 'social_links',
      'label' => $value,
    ]);
  });
});
