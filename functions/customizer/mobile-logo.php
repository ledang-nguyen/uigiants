<?php
/**
 * Register Mobile Logo
 */
add_action('customize_register', function () {
  global $wp_customize;

  $wp_customize->add_setting('mobile_logo', [
    'default-image' => '',
    'transport' => 'refresh',
  ]);

  $wp_customize->add_control(
      new WP_Customize_Image_Control($wp_customize, 'mobile_logo', [
        'label' => __('Mobile Logo'),
        'section' => 'title_tagline',
        'settings' => 'mobile_logo',
        'priority' => 9
      ])
  );
});
