<?php
/**
 * Register Alternative Logo
 */
add_action('customize_register', function () {
  global $wp_customize;

  $wp_customize->add_setting('alt_logo', [
    'default-image' => '',
    'transport' => 'refresh',
  ]);

  $wp_customize->add_control(
      new WP_Customize_Image_Control($wp_customize, 'alt_logo', [
        'label' => __('Alternative Logo'),
        'section' => 'title_tagline',
        'settings' => 'alt_logo',
        'priority' => 9
      ])
  );
});
