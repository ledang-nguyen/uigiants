<?php
class Socials extends WP_Widget {
  public function __construct() {
    parent::__construct(false, 'Socials');
  }

  public function widget($args, $instance) {
    get_widget_template($this, $args, $instance);
  }

  public function update($new_instance, $old_instance) {
    $instance = [];
    $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

    return $instance;
  }

  public function form($instance) {
    $title = !empty($instance['title']) ? $instance['title'] : null; ?>

    <p>
      <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:'); ?></label>
      <input class="widefat"
             id="<?php echo esc_attr($this->get_field_id('title')); ?>"
             name="<?php echo esc_attr($this->get_field_name('title')); ?>"
             type="text"
             value="<?php echo esc_attr($title); ?>">
    </p>
    <?php
  }
}

register_widget('Socials');
