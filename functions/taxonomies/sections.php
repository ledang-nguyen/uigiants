<?php
/**
 * Taxonomy: Sections.
 */

function cptui_register_my_taxes() {
  $labels = [
    "name" => __("Sections", "custom-post-type-ui"),
    "singular_name" => __("Section", "custom-post-type-ui"),
    "menu_name" => __("Sections", "custom-post-type-ui"),
    "all_items" => __("All Sections", "custom-post-type-ui"),
    "edit_item" => __("Edit Section", "custom-post-type-ui"),
    "view_item" => __("View Section", "custom-post-type-ui"),
    "update_item" => __("Update Section name", "custom-post-type-ui"),
    "add_new_item" => __("Add new Section", "custom-post-type-ui"),
    "new_item_name" => __("New Section name", "custom-post-type-ui"),
    "parent_item" => __("Parent Section", "custom-post-type-ui"),
    "parent_item_colon" => __("Parent Section:", "custom-post-type-ui"),
    "search_items" => __("Search Sections", "custom-post-type-ui"),
    "popular_items" => __("Popular Sections", "custom-post-type-ui"),
    "separate_items_with_commas" => __("Separate Sections with commas", "custom-post-type-ui"),
    "add_or_remove_items" => __("Add or remove Sections", "custom-post-type-ui"),
    "choose_from_most_used" => __("Choose from the most used Sections", "custom-post-type-ui"),
    "not_found" => __("No Sections found", "custom-post-type-ui"),
    "no_terms" => __("No Sections", "custom-post-type-ui"),
    "items_list_navigation" => __("Sections list navigation", "custom-post-type-ui"),
    "items_list" => __("Sections list", "custom-post-type-ui"),
  ];

  $args = [
    "label" => __("Sections", "custom-post-type-ui"),
    "labels" => $labels,
    "public" => true,
    "publicly_queryable" => true,
    "hierarchical" => true,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => ['slug' => 'section', 'with_front' => true,],
    "show_admin_column" => true,
    "show_in_rest" => true,
    "rest_base" => "section",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
  ];
  register_taxonomy("section", ["help_center"], $args);
}

add_action('init', 'cptui_register_my_taxes');
