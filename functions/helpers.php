<?php
/**
 * Returns an instance of the BemMenu class with the following arguments
 */
if (!function_exists('get_bem_menu')) {
  function get_bem_menu($location = 'main_menu', $css_class_prefix = 'mainMenu', $css_class_modifiers = null, $depth = 2) {
    if (!has_nav_menu($location)) return;

    // Check to see if any css modifiers were supplied
    if ($css_class_modifiers) {
      if (is_array($css_class_modifiers)) {
        $modifiers = implode(' ', $css_class_modifiers);
      } elseif (is_string($css_class_modifiers)) {
        $modifiers = ' ' . $css_class_modifiers;
      }
    } else {
      $modifiers = '';
    }

    $args = [
      'theme_location'    => $location,
      'container'         => false,
      'items_wrap'        => '<ul class="' . $css_class_prefix . $modifiers . '">%3$s</ul>',
      'walker'            => new BemMenu($css_class_prefix, true),
      'link_before'       => '<span>',
      'link_after'        => '</span>',
      'depth'             => $depth,
    ];

    return wp_nav_menu($args);
  };
}

/**
 * Includes a widget template to widget class
 */
if (!function_exists('get_widget_template')) {
  function get_widget_template($widget, $args, $instance) {
    $template_name = sanitize_title($widget->name);
    $template = locate_template("views/widgets/{$template_name}.php");
    $title = $instance['title'];

    echo wp_kses_post($args['before_widget']);
    echo (!empty($title) ? wp_kses_post($args['before_title'] . esc_attr($title) . wp_kses_post($args['after_title'])) : null);

    if ($template) {
      include $template;
    }

    echo wp_kses_post($args['after_widget']);
  }
}

/**
 * Render the link from ACF Link field
 */
if (!function_exists('get_field_link')) {
  function get_field_link($link, $class, $default = 'Learn More') {
    if (empty($link) && !is_array($link)) return false;

    $link_title = !empty($link['title']) ? $link['title'] : $default;

    $output = "<a ";
    $output .= !empty($class) ? "class='{$class}'" : null;
    $output .= "href='{$link['url']}'";
    $output .= !empty($link['target']) ? " target='_blank' rel='noopener noreferrer'" : null;
    $output .= !empty($link['download']) ? " download" : null;
    $output .= "><span>{$link_title}</span></a>";

    echo $output; // WPCS: Xss ok
  }
}

/**
 * Returns an array of assets
 */
if (!function_exists('get_assets_url')) {
  function get_assets_url($manifest = 'mix-manifest.json') {
    global $func_error;

    $manifest_path = get_template_directory() . '/' . THEME_ASSETS_PATH . '/' . $manifest;

    if (!is_file($manifest_path)) {
      $func_error(sprintf(__('The assets manifest file does not exist..')), 'File not found');

      return false;
    }

    $assets = json_decode(file_get_contents($manifest_path), true); // @codingStandardsIgnoreLine

    return $assets;
  }
}

/**
 * Get customizer field group
 */
if (!function_exists('get_customizer_field_group')) {
  function get_customizer_field_group($field_group, $exclude = null) {
    $fields = get_theme_mod($field_group, []);

    $filtered = array_filter($fields, function ($field) {
      return empty($fields[$field]);
    }, ARRAY_FILTER_USE_KEY);

    if (!empty($exclude)) {
      $filtered = array_filter($fields, function ($field) use ($exclude) {
        return $field !== $exclude;
      }, ARRAY_FILTER_USE_KEY);
    }

    return $filtered;
  }
}

/**
 * Custom Excerpt
 * @param $count
 * @param string $content
 * @param string $suffix
 * @return string
 */
if (!function_exists('get_excerpt')) {
  function get_excerpt($count, $suffix = '...', $content = '') {
    $excerpt = !empty($content) ? $content : get_the_content();
    $excerpt = wp_strip_all_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = $excerpt . $suffix;
    return $excerpt;
  }
}

/**
 * Mobile Logo
 */
if (!function_exists('the_mobile_logo')) {
  function the_mobile_logo() {
    if (empty(get_theme_mod('mobile_logo'))) {
      return;
    }

    $logo_id = attachment_url_to_postid(get_theme_mod('mobile_logo'));

    echo wp_get_attachment_image($logo_id, 'medium');
  }
}

/**
 * Get current template slug
 */
if (!function_exists('get_current_template_slug')) {
  function get_current_template_slug() {
    $current_template_slug = explode("/", get_page_template_slug());
    $current_template_slug = end($current_template_slug);

    return str_replace(".php", "", $current_template_slug);
  }
}

/**
 * Compare template slug with the current one
 */
if (!function_exists('is_template')) {
  function is_template($template_slug = '') {
    $current_template_slug = get_current_template_slug();

    return strcasecmp($current_template_slug, $template_slug) === 0;
  }
}
