<?php
/**
 * Add Google Maps API Key
 */
add_action('acf/init', function () {
  acf_update_setting('google_api_key', '');
});

/**
 * Defer js loading
 */
function add_async_and_defer_attribute($tag, $handle) {
  if ('jquery' === $handle) {
    return $tag;
  }

  return str_replace('src', 'defer onload="" src', $tag);
}

if (!is_admin()) {
  add_filter('script_loader_tag', 'add_async_and_defer_attribute', 10, 2);
}
