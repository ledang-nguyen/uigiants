<?php
/**
 * Post Type: Help Center.
 */

function cptui_register_my_cpts() {
  $help_center_title = get_post_meta(442, '_aioseop_title', true);
  $help_center_desc = get_post_meta(442, "_aioseop_description", true);

  $labels = [
    "name" => __("Articles", "custom-post-type-ui"),
    "singular_name" => __("Article", "custom-post-type-ui"),
    "menu_name" => __("Help Center", "custom-post-type-ui"),
    "all_items" => __("All Articles", "custom-post-type-ui"),
    "add_new" => __("Add new", "custom-post-type-ui"),
    "add_new_item" => __("Add new Article", "custom-post-type-ui"),
    "edit_item" => __("Edit Article", "custom-post-type-ui"),
    "new_item" => __("New Article", "custom-post-type-ui"),
    "view_item" => __("View Article", "custom-post-type-ui"),
    "view_items" => __("View Articles", "custom-post-type-ui"),
    "search_items" => __("Search Articles", "custom-post-type-ui"),
    "not_found" => __("No Articles found", "custom-post-type-ui"),
    "not_found_in_trash" => __("No Articles found in trash", "custom-post-type-ui"),
    "parent" => __("Parent Article:", "custom-post-type-ui"),
    "featured_image" => __("Featured image for this Article", "custom-post-type-ui"),
    "set_featured_image" => __("Set featured image for this Article", "custom-post-type-ui"),
    "remove_featured_image" => __("Remove featured image for this Article", "custom-post-type-ui"),
    "use_featured_image" => __("Use as featured image for this Article", "custom-post-type-ui"),
    "archives" => __("Article archives", "custom-post-type-ui"),
    "insert_into_item" => __("Insert into Article", "custom-post-type-ui"),
    "uploaded_to_this_item" => __("Upload to this Article", "custom-post-type-ui"),
    "filter_items_list" => __("Filter Articles list", "custom-post-type-ui"),
    "items_list_navigation" => __("Articles list navigation", "custom-post-type-ui"),
    "items_list" => __("Articles list", "custom-post-type-ui"),
    "attributes" => __("Articles attributes", "custom-post-type-ui"),
    "name_admin_bar" => __("Article", "custom-post-type-ui"),
    "item_published" => __("Article published", "custom-post-type-ui"),
    "item_published_privately" => __("Article published privately.", "custom-post-type-ui"),
    "item_reverted_to_draft" => __("Article reverted to draft.", "custom-post-type-ui"),
    "item_scheduled" => __("Article scheduled", "custom-post-type-ui"),
    "item_updated" => __("Article updated.", "custom-post-type-ui"),
    "parent_item_colon" => __("Parent Article:", "custom-post-type-ui"),
    'archive_seo_title' => $help_center_title,
    'archive_meta_desc' => $help_center_desc
  ];

  $args = [
    "label" => __("Articles", "custom-post-type-ui"),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "help-center", "with_front" => false],
    "query_var" => true,
    "menu_position" => 5,
    "menu_icon" => "dashicons-book",
    "supports" => ["title", "editor", "revisions", "page-attributes"],
    "taxonomies" => ["section"],
  ];

  register_post_type("help_center", $args);
}

add_action('init', 'cptui_register_my_cpts');
