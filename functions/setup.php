<?php
/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
  $assets = get_assets_url();

  wp_enqueue_style('main', get_template_directory_uri() . '/assets/build' . $assets['/css/bundle.css'], null, null, 'all');
  wp_enqueue_script('main', get_template_directory_uri() . '/assets/build' . $assets['/js/bundle.js'], null, null, true);

  if (!is_admin()) {
    wp_deregister_script('jquery-core');
    wp_deregister_script('jquery-migrate');
    wp_register_script('jquery-core', get_template_directory_uri() . '/assets/build' . $assets['/js/jquery.min.js'], [], '3.5.1');
    wp_register_script('jquery-migrate', get_template_directory_uri() . '/assets/build' . $assets['/js/jquery-migrate.min.js'], [], '3.3.0');
  }

  /**
   * Remove Gutenberg script from loading
   */
  wp_dequeue_style('wp-block-library');

  wp_localize_script('main', 'wp', [
    'endpoint' => esc_url_raw(rest_url('wp/v2')),
  ]);
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
  /**
   * Enable plugins to manage the document title
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
   */
  add_theme_support('title-tag');

  /**
   * Register navigation menus
   * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
   */
  register_nav_menus([
    'main-nav' => __('Main Navigation'),
    'privacy-terms' => __('Privacy and Terms'),
    'footer-nav' => __('Footer Navigation'),
  ]);

  /**
   * Enable post thumbnails
   * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
   */
  add_theme_support('post-thumbnails');

  /**
   * Enable HTML5 markup support
   * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
   */
  add_theme_support('html5');

  /**
   * Add custom logo support
   * @link https://developer.wordpress.org/themes/functionality/custom-logo/
   */
  add_theme_support('custom-logo', [
    'flex-width'  => true,
    'flex-height' => true,
    'uploads'     => true,
    'header-text' => false,
  ]);

  /**
   * Add Image sizes
   */
  add_image_size('blog-post-small', 740, 520, ['center', 'center']);
  add_image_size('single-posts', 740, 570, ['center', 'center']);
});

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
  $config = [
    'before_widget' => '<aside class="widget %1$s %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>'
  ];

  register_sidebar([
    'name' => __('Footer (Default)'),
    'id' => 'footer'
  ] + $config);

  register_sidebar([
    'name' => __('Footer Top (Blog)'),
    'id' => 'footer-top'
  ] + $config);
});

/**
 * Add custom logo if exists
 */
add_action('login_enqueue_scripts', function () {
  $logo_id = get_theme_mod('custom_logo');
  $logo_url = wp_get_attachment_image_url($logo_id, 'full');

  if (empty($logo_id)) return;

  $styles = "<style>
    .login #login h1 a {
      background-image: none, url(${logo_url});
      background-size: contain;
      width: auto;
      max-height: 400px;
    }
  </style>";

  echo wp_kses($styles, ['style' => true]);
});

/**
 * Change logo url
 */
add_filter('login_headerurl', function () {
  $site_url = home_url();

  return ($site_url);
});

/**
 * Extend WordPress search to include custom fields
 *
 * Join posts and postmeta tables
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
add_filter('posts_join', function ($join) {
  global $wpdb;

  if (is_search()) {
    $join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  }

  return $join;
});

/**
 * Modify the search query with posts_where
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
add_filter('posts_where', function ($where) {
  global $pagenow, $wpdb;

  if (is_search()) {
    $where = preg_replace('/\(\s*' . $wpdb->posts . '.post_title\s+LIKE\s*(\'[^\']+\')\s*\)/', '(' . $wpdb->posts . '.post_title LIKE $1) OR (' . $wpdb->postmeta . '.meta_value LIKE $1)', $where);
  }

  return $where;
});

/**
 * Prevent duplicates
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
add_filter('posts_distinct', function ($where) {
  global $wpdb;

  if (is_search()) {
    return 'DISTINCT';
  }

  return $where;
});

/**
 * Add shortcode for current year
 */
add_shortcode('year', function () {
  return date('Y');
});

/**
 * Allow using shortcodes in Text Widgets
 */
add_filter('widget_text', 'do_shortcode');

/**
 * Unregister unnecessary widgets
 */
add_action('widgets_init', function () {
  unregister_widget('WP_Widget_Pages');
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Archives');
  unregister_widget('WP_Widget_Links');
  unregister_widget('WP_Widget_Media_Audio');
  unregister_widget('WP_Widget_Media_Image');
  unregister_widget('WP_Widget_Media_Video');
  unregister_widget('WP_Widget_Media_Gallery');
  unregister_widget('WP_Widget_Meta');
  unregister_widget('WP_Widget_Search');
  unregister_widget('WP_Widget_Categories');
  unregister_widget('WP_Widget_Recent_Posts');
  unregister_widget('WP_Widget_Recent_Comments');
  unregister_widget('WP_Widget_RSS');
  unregister_widget('WP_Widget_Tag_Cloud');
  unregister_widget('WP_Nav_Menu_Widget');
});

/**
 * Hide Custom Fields section in Admin menu
 */
//add_filter('acf/settings/show_admin', '__return_false');

/**
 * Add taxonomy to post meta fields
 */
add_action('init', function () {
  $articles = new WP_Query([
    'post_type' => 'help_center',
    'posts_per_page' => -1,
  ]);

  while ($articles->have_posts()) : $articles->the_post();
    $current_taxonomy = current(get_the_terms(get_the_ID(), 'section'));

    update_post_meta(get_the_ID(), '_current_taxonomy', $current_taxonomy->name);
  endwhile;
}, 100);

function enqueue_fancybox() {
  $assets = get_assets_url();

  wp_enqueue_style('fancybox', get_template_directory_uri() . '/assets/build' . $assets['/css/jquery.fancybox.min.css'], null, null, 'all');
  wp_enqueue_script('fancybox', get_template_directory_uri() . '/assets/build' . $assets['/js/jquery.fancybox.min.js'], null, null, true);
}

/**
 * CPT Archive page SEO Title
 */
add_filter('aioseop_title', 'dd_rewrite_custom_titles');
function dd_rewrite_custom_titles($title) {
  if (is_post_type_archive()) {
    $post_type = get_post_type_object(get_post_type());
    if ($post_type->labels->archive_seo_title) {
      $title = $post_type->labels->archive_seo_title;
    }
  }
  return $title;
}

/**
 * CPT Archive page meta Description
 */
add_filter('aioseop_description', 'filter_aioseop_description');
function filter_aioseop_description($description) {
  if (is_post_type_archive()) {
    $post_type = get_post_type_object(get_post_type());
    if ($post_type->labels->archive_meta_desc) {
      $description = $post_type->labels->archive_meta_desc;
    }
  }
  return $description;
}
