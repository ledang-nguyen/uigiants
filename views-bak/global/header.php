<?php
/**
 * Header
 */
?>

<?php
$no_menu = !is_404() &&
          !is_template('thank-you');
$is_blog = is_home() || is_archive() || is_single() || is_search();
$alt_logo_id = attachment_url_to_postid(get_theme_mod('alt_logo'));
$mobile_logo_id = attachment_url_to_postid(get_theme_mod('mobile_logo'));

$alt_logo = wp_get_attachment_image($alt_logo_id);
$mobile_logo = wp_get_attachment_image($mobile_logo_id);
?>

<div class="Page">
  <header class="Header">
    <div class="Header-wrapper">
      <div class="Header-left">
        <div class="Header-logo">
          <?php if ($is_blog) : ?>
            <a href="<?php echo esc_url(home_url()); ?>">
              <?php echo wp_kses_post($alt_logo); ?>
            </a>

            <a class="Header-logoMobile" href="<?php echo esc_url(home_url()); ?>">
              <?php echo wp_kses_post($mobile_logo); ?>
            </a>
          <?php else : ?>
            <?php the_custom_logo(); ?>
          <?php endif; ?>
        </div>
        <?php if ($is_blog) : ?>
          <div class="Header-blog">
            <ul class="MainNav">
              <li class="MainNav-item">
                <a class="MainNav-link" href="<?php echo esc_url(home_url('/blog')); ?>">Blog</a>
              </li>
            </ul>
          </div>
        <?php endif; ?>
      </div>
      <?php if ($no_menu) : ?>
        <div class="Header-right">
          <?php if ($is_blog) : ?>
            <?php get_search_form(); ?>
          <?php endif; ?>
          
          <?php get_bem_menu('main-nav', 'MainNav'); ?>

          <?php if ($is_blog) : ?>
            <button class="Header-subscribe js-subscribe">Subscribe</button>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
  </header>
