<?php
/**
 * Footer
 */
?>

<?php
$no_footer = !is_404() &&
            !is_template('thank-you');
$is_blog = is_home() || is_archive() || is_single() || is_search();
?>

  <?php if ($no_footer) : ?>
    <footer class="Footer">
      <?php if ($is_blog) : ?>
        <div class="Footer-header">
          <?php dynamic_sidebar('footer-top'); ?>
        </div>
      <?php endif; ?>

      <div class="Footer-wrapper">
        <?php dynamic_sidebar('footer'); ?>
      </div>

      <?php if (have_posts() && $is_blog) : ?>
        <div class="TopBtn">
          <div class="TopBtn-wrapper">
            <button class="TopBtn-button">
              <span>Top</span>
            </button>
          </div>
        </div>
      <?php endif; ?>
    </footer>
  <?php endif; ?>

  <div class="Cookies">
    <div class="Cookies-wrapper">
      <span class="Cookies-text">We use cookies to make sure you get the best experience on our website.</span>
      <button class="Cookies-button">Got it</button>
    </div>
  </div>
</div>
