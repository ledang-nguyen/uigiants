<?php
/**
 * Plain Text Component (Home)
 */
?>

<?php
$heading = get_sub_field('title');
$text = get_sub_field('text');
?>

<?php if (!empty($heading) && !empty($text)) : ?>
  <section class="PlainText">
    <div class="PlainText-wrapper">
      <h2 class="PlainText-title">
        <?php echo esc_html($heading); ?>
      </h2>
      <div class="PlainText-text">
        <?php echo wp_kses_post($text); ?>
      </div>
    </div>
  </section>
<?php endif; ?>
