<?php
/**
 * Text With Media Component (Home)
 */
?>

<?php
$position_class = get_sub_field('position');
$type_class = get_sub_field('type');
$class_array = implode(' ', [$position_class, $type_class]);

$image = get_sub_field('image') ? wp_get_attachment_image(get_sub_field('image'), 'full') : null;

$heading = get_sub_field('title');
$text = get_sub_field('text');

$logos = get_sub_field('logos');
?>

<?php if (!empty($image)) : ?>
  <section class="TextWithMedia <?php echo esc_attr($class_array); ?>">
    <div class="TextWithMedia-wrapper">
      <div class="TextWithMedia-content">
        <h2 class="TextWithMedia-contentTitle">
          <?php echo esc_html($heading); ?>
        </h2>
        <div class="TextWithMedia-contentText">
          <?php echo wp_kses_post($text); ?>
        </div>

        <?php if (!empty($logos)) : ?>
          <div class="TextWithMedia-logos">
            <?php foreach ($logos as $logo) :
              $logo_image = $logo['image'] ? wp_get_attachment_image($logo['image'], 'full') : null;
              ?>
              <div class="TextWithMedia-logosItem">
                <?php echo wp_kses_post($logo_image); ?>
              </div>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>
      <div class="TextWithMedia-media">
        <div class="TextWithMedia-mediaBody">
          <?php echo wp_kses_post($image); ?>
        </div>
      </div>
    </div>
  </section>
<?php endif;
