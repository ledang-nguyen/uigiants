<?php
/**
 * Form Component (Home)
 */
?>

<?php
$form_id = get_sub_field('form_id');
?>

<?php if (!empty($form_id)) : ?>
  <section class="Form">
    <div class="Form-wrapper">
      <?php echo do_shortcode("[contact-form-7 id=\"${form_id}\"]"); ?>
    </div>
  </section>
<?php endif; ?>
