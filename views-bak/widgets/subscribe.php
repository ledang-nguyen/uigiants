<?php
/**
 * Subscribe Form Widget
 */
?>

<div class="Subscribe">
  <div class="Subscribe-wrapper">
    <?php echo do_shortcode("[contact-form-7 id=\"31\"]"); ?>
  </div>
</div>
