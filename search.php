<?php
/**
 * The main template file
 */
?>

<?php get_header(); ?>

<?php
$is_blog = is_home() || is_post_type_archive('post') || is_singular('post') || (isset($_GET['post_type']) && $_GET['post_type'] == 'post');
$is_help_center = is_singular('help_center') || is_post_type_archive('help_center') || (isset($_GET['post_type']) && $_GET['post_type'] == 'help_center');
?>

<?php if (have_posts()) :
  $first_post = true;
  ?>
  <main class="Main">
    <div class="Main-components">
      <div class="Blog">
        <div class="Blog-wrapper">
          <div class="Blog-body">
            <?php while (have_posts()) : the_post();
              $first_post_class = $first_post && !is_search() ? '-big' : null;
              $image = get_the_post_thumbnail_url(get_the_ID(),'full');
              $first_post_style = $first_post && !is_search() ? 'style="background-image: url(' . esc_url($image) . ')"' : null;
              ?>
              <article class="Blog-item <?php echo esc_attr($first_post_class); ?> Blog-item-<?php echo esc_attr(get_the_ID()); ?>" <?php echo wp_kses_post($first_post_style); ?>>
                <div class="Blog-itemInner">
                  <?php if (!is_search()) : ?>
                    <div class="Blog-itemImage">
                      <a class="Blog-itemImageLink" href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('blog-post-small'); ?>
                      </a>
                    </div>
                  <?php endif; ?>

                  <div class="Blog-itemContent">
                    <h3 class="Blog-itemTitle">
                      <a class="Blog-itemTitleLink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>
                    <div class="Blog-itemExcerpt">
                      <?php the_excerpt(); ?>
                    </div>
                    <a class="Blog-itemLink" href="<?php the_permalink(); ?>">
                      <span>Read More</span>
                    </a>
                  </div>
                </div>
              </article>
              <?php $first_post = false;
            endwhile; ?>
          </div>
          <?php if (get_the_posts_pagination()) : ?>
            <div class="Blog-footer">
              <div class="Blog-pagination">
                <?php the_posts_pagination(); ?>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </main>
<?php else : ?>
  <main class="Main">
    <div class="Main-components">
      <div class="NoResults">
        <div class="NoResults-wrapper">
          <span class="NoResults-title">No Results Found for</span>
          <span class="NoResults-query"><?php the_search_query(); ?></span>
          <?php if ($is_help_center) : ?>
            <a class="NoResults-back" href="<?php echo esc_url(home_url('/help-center')); ?>">Back to Help Center</a>
          <?php else : ?>
            <a class="NoResults-back" href="<?php echo esc_url(home_url('/blog')); ?>">Back to Blog</a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </main>
<?php endif; ?>

<?php get_footer();
