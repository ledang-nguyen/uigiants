<?php
/**
 * The front page template
 */
?>

<?php get_header(); ?>

<main class="Main">
  <div class="Main-components">

    <?php while (have_rows('components')) :
      the_row();
      $section_name = str_replace('_', '-', get_row_layout()); ?>

      <?php get_template_part("views/components/home/{$section_name}"); ?>

    <?php endwhile; ?>
    
  </div>
</main>

<?php get_footer();
