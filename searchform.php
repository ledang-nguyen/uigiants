<?php
/**
 * Template for displaying search forms
 */
?>

<?php
$is_blog = is_home() || is_post_type_archive('post') || is_singular('post') || (isset($_GET['post_type']) && $_GET['post_type'] == 'post');
$is_help_center = is_singular('help_center') || is_post_type_archive('help_center') || (isset($_GET['post_type']) && $_GET['post_type'] == 'help_center');
$placeholder = $is_help_center ? 'Search Help Center' : 'Search';
?>

<form class="SearchForm" method="get" action="<?php echo esc_url(home_url('/')); ?>">
  <button class="SearchForm-submit" type="submit"></button>
  <input type="text" class="SearchForm-input" name="s" placeholder="<?php esc_attr_e($placeholder, 'uigiants'); ?>" value="<?php the_search_query(); ?>" />
  <?php if ($is_blog) : ?>
    <input type="hidden" name="post_type" value="post" />
  <?php elseif ($is_help_center) : ?>
    <input type="hidden" name="post_type" value="help_center" />
  <?php endif; ?>
</form>
