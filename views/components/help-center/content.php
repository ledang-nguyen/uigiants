<?php
/**
 * Content Component (Help Center)
 */
?>

<?php
$text = str_replace("-&gt;", "<i class='Content-arrow'></i>", get_sub_field('text'));
?>

<div class="Content">
  <div class="Content-wrapper">
    <?php echo apply_filters('the_content', $text); ?>
  </div>
</div>
