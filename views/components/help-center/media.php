<?php
/**
 * Media Component (Help Center)
 */
?>

<?php
$media_type = get_sub_field('type');
$image = get_sub_field('image') ? wp_get_attachment_image(get_sub_field('image'), 'full') : null;
$video_mp4 = get_sub_field('video_mp4');
?>

<div class="Media">
  <div class="Media-wrapper">
    <?php if ($media_type === 'Image') : ?>
      <?php echo wp_kses_post($image); ?>
    <?php else : ?>
      <video preload="metadata" muted playsinline webkit-playsinline>
        <source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4">
      </video>
    <?php endif; ?>
  </div>
</div>
