<?php
/**
 * Important Component (Blog)
 */
?>

<?php
$text = get_sub_field('text');
?>

<div class="Important">
  <div class="Important-wrapper">
    <div class="Important-inner">
      <div class="Important-body">
        <?php echo apply_filters('the_content', $text); ?>
      </div>
    </div>
  </div>
</div>
