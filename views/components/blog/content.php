<?php
/**
 * Content Component (Blog)
 */
?>

<?php
$text = get_sub_field('text');
?>

<div class="Content">
  <div class="Content-wrapper">
    <?php echo apply_filters('the_content', $text); ?>
  </div>
</div>
