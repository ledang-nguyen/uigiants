<?php
/**
 * Welcome Component (Home)
 */
?>

<?php
$caption = get_sub_field('caption');
$heading = get_sub_field('heading');
$text = get_sub_field('text');
$form_id = get_sub_field('form_id');
$image_url = get_sub_field('image') ? wp_get_attachment_image_url(get_sub_field('image'), 'full') : null;
$bg_style = $image_url ? 'style="background-image: url(' . esc_url($image_url) . ')"' : null;
?>

<section class="Welcome" <?php echo wp_kses_post($bg_style); ?>>
  <div class="Welcome-wrapper">
    <div class="Welcome-inner">
      <?php if ($caption) : ?>
        <span class="Welcome-caption">
          <?php echo esc_html($caption); ?>
        </span>
      <?php endif; ?>

      <h1 class="Welcome-title">
        <?php echo esc_html($heading); ?>
      </h1>

      <?php if (!empty($text)) : ?>
        <div class="Welcome-text">
          <?php echo wp_kses_post($text); ?>
        </div>
      <?php endif; ?>

      <?php if ($form_id) : ?>
        <div class="Welcome-form">
          <?php echo do_shortcode("[contact-form-7 id=\"${form_id}\"]"); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
