<?php
/**
 * Blocks With Icons Component (Home)
 */
?>

<?php
$heading = get_sub_field('heading');
$blocks = get_sub_field('blocks');
?>

<?php if (!empty($blocks)) : ?>
  <section class="BlocksWithIcons">
    <?php if (!empty($heading)) : ?>
      <div class="BlocksWithIcons-header">
        <h2 class="BlocksWithIcons-title">
          <?php echo esc_html($heading); ?>
        </h2>
      </div>
    <?php endif; ?>

    <div class="BlocksWithIcons-wrapper">
      <?php foreach ($blocks as $block) :
        $icon = !empty($block['icon']) ? wp_get_attachment_image($block['icon'], 'full') : null;
        $block_title = $block['title'];
        $text = $block['text'];
        ?>
        <div class="BlocksWithIcons-item">
          <div class="BlocksWithIcons-itemIcon">
            <?php echo wp_kses_post($icon); ?>
          </div>
          <div class="BlocksWithIcons-itemContent">
            <h3 class="BlocksWithIcons-itemTitle">
              <?php echo esc_html($block_title); ?>
            </h3>
            <div class="BlocksWithIcons-itemText">
              <?php echo wp_kses_post($text); ?>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </section>
<?php endif; ?>
