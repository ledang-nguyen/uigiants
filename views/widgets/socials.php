<?php
/**
 * Socials Widget
 */
?>

<?php
$socials = get_theme_mod('social_links');
?>

<?php if (!empty($socials)) : ?>
  <div class="Socials">
    <ul class="Socials-wrapper">
      <?php foreach ($socials as $key => $social) : ?>
        <?php if (!empty($social)) : ?>
          <li class="Socials-item -<?php echo esc_attr($key); ?>">
            <a class="Socials-itemLink" href="<?php echo esc_url($social); ?>" target="_blank" rel="noopener">
              <i class="icon-<?php echo esc_attr($key); ?>"></i>
            </a>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
