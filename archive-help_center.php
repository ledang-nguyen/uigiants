<?php
/**
 * The template for displaying Help Center single post
 *
 * Template Name: Help Center Archive
 */
?>

<?php get_header(); ?>

<?php
enqueue_fancybox();

$terms = get_terms([
  'taxonomy' => 'section',
  'hide_empty' => false,
]);

$heading = get_field('title', 442);
$text = get_field('text', 442);
$form_id = get_field('form_id', 442);
$thank_you = get_field('thank_you', 442);
$socials = $thank_you['socials'];
?>

  <main class="Main">
    <div class="Main-wrapper">
      <div class="Main-sidebar">
        <div class="Sidebar">
          <div class="Sidebar-header">
            <span class="Sidebar-title">
              <?php echo esc_html($heading); ?>
            </span>
          </div>
          <div class="Sidebar-body">
            <?php foreach ($terms as $term_item) :
              $articles_args = [
                'post_type' => 'help_center',
                'posts_per_page' => -1,
                'tax_query' => [
                  [
                    'taxonomy' => 'section',
                    'field'    => 'term_id',
                    'terms'    => $term_item->term_id,
                  ],
                ],
                'order' => 'ASC',
                'orderby' => 'menu_order'
              ];

              $articles = new WP_Query($articles_args);
              ?>
              <div class="Sidebar-item">
                <span class="Sidebar-itemHeading"><?php echo esc_html($term_item->name); ?></span>

                <ul class="Sidebar-list">
                  <?php while ($articles->have_posts()) : $articles->the_post(); ?>
                    <li class="Sidebar-listItem">
                      <a class="Sidebar-listItemLink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                  <?php endwhile; wp_reset_postdata(); ?>
                </ul>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>

      <div class="Main-body">
        <div class="ContactUs">
          <div class="ContactUs-wrapper">
            <div class="ContactUs-header">
              <h1 class="ContactUs-title">
                <?php echo esc_html($heading); ?>
              </h1>
              <div class="ContactUs-text">
                <?php echo wp_kses_post($text) ?>
              </div>
            </div>
            <div class="ContactUs-body">
              <div class="ContactUs-form">
                <?php echo do_shortcode("[contact-form-7 id=\"${form_id}\"]"); ?>
              </div>
              <div class="ContactUs-thankYou">
            <span class="ContactUs-thankYouTitle">
              <?php echo esc_html($thank_you['title']); ?>
            </span>
                <div class="ContactUs-thankYouText">
                  <?php echo wp_kses_post($thank_you['text']); ?>
                </div>

                <?php if (!empty($socials)) : ?>
                  <ul class="ContactUs-socials">
                    <?php foreach ($socials as $social) :
                      $social_type = $social['type'];
                      $social_link = $social['link'];
                      ?>
                      <li class="ContactUs-socialsItem">
                        <?php get_field_link($social_link, "ContactUs-socialsLink -${social_type}"); ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>

                <a href="<?php echo esc_url(home_url('/help-center')); ?>" class="ContactUs-thankYouBack">Submit Another Email</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

<?php get_footer();
