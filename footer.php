<?php
/**
 * The template for displaying the footer
 */
?>

<?php get_template_part('views/global/footer'); ?>

<?php wp_footer(); ?>

</body>
</html>
